// [Section] Comparison Query Operators

    // $gt
    // $gte
    /* 
        - it allows us to find document that have field number value greater than or equal to a specified value
        Syntax:
            db.collectionName.find({field: {$gt : value}});
            db.collectionName.find({field: {$gte : value}});
    */

    db.users.find({
        age: {
            $gt:50 
        }
    }).pretty();

    db.users.find({
        age: {
            $gt:50 
        }
    }).pretty(); 


    // $lt
    // $lte
    /* 
        - it allows us to find document that have field number value less than or equal to a specified value
        Syntax:
            db.collectionName.find({field: {$lt : value}});
            db.collectionName.find({field: {$lte : value}});
    */
   
    db.users.find({
        age: {
            $lt:50 
        }
    }).pretty();

    db.users.find({
        age: {
            $lte:76
        }
    }).pretty();


    // ne
    /* 
        - it allows us to find document that have field number value not equal to a specified value
        Syntax:
            db.collectionName.find({field: {$ne : value}});
    */

    db.users.find({
        age: {
            $ne:0 
        }
    }).pretty();


    // $in
    /* 
        - it allows us to find document with specific match criteria one field using different
        Syntax:
            db.collectionName.find({field: {$in : [valueA, valueB]}});
    */

    db.users.find({
        lastName: {
            $in: ["Doe", "Hawking"]
        },
        firstName: "Stephen"
    }).pretty();

    db.users.find({
        courses: {
            $in: ["CSS", "JavaSript"]
        }
    }).pretty();


// [Section] Logical Query Operators

    // $or
    /* 
        - it allows us to find documents that match a single criteria from multiple provided search criteria
        Syntax: 
            db.collectionName.find({$or: [{fieldA:valueA}, {fieldB:valueB}]});
    */

    db.users.find({
        $or: [
            {firstName:"Neil"},
            {age: 25}
        ]
    }).pretty();

    db.users.find({
        $or: [
            {firstName:"Neil"},
            {age: {$gt:30}}
        ]
    }).pretty();


    // $and
    /* 
        - it allows us to find documents that matching multiple provided criteria
        Syntax: 
            db.collectionName.find({$and: [{fieldA:valueA}, {fieldB:valueB}]});
    */

    db.users.find({
        $and: [
            {firstName:"Neil"},
            {age: {$gt:30}}
        ]
    }).pretty();

    db.users.find({
        $and: [
            {firstName:"Neil"},
            {age: 25}
        ]
    }).pretty();

    db.users.find({
        firstName:"Neil",
        age: {$gte:30}
    }).pretty();

// [Section] Field Projection

    // inclusion
    /* 
        - it allows us to include / add specific field only when retrieving documents
        Syntax:
            db.collectionName.find({criteria}, {fields: 1})
    */

    db.users.find(
        {
            $and: [
                {firstName:"Neil"},
                {age: {$gt:30}}
            ]
        }, 
        {
            firstName: true,
            lastName: "----",
            age: 1
        }
    ).pretty();

    // exclusion
    /* 
        - it allows us to exclude / remove specific field only when retrieving documents
        Syntax:
            db.collectionName.find({criteria}, {fields: 0})
    */

    db.users.find(
        {
            firstName:"Neil",
            age: {$gte: 25}
        }, 
        {
            age: false,
            courses: 0
        }
    ).pretty();

    // Returning specific field in embedded documents

    db.users.find(
        {
            firstName:"Jane"
        }, 
        {
            "contact.phone": true,
            _id: false
        }
    ).pretty();

    db.users.find(
        {
            firstName:"Jane"
        }, 
        {
            "contact.phone": false
        }
    ).pretty();

// [Section] Evaluation Query Operator

    /* 
        - it allows us to find documents that match a specific string pattern using regular expression
        Syntax:
            db.users.find({field: {$regex:"pattern", $options:"$optionValue"})
    */

    // Case sensitive
    db.users.find(
        {
            firstName: {
                $regex : "N",
                $options: "$i"
            }
        }
    ).pretty();

    // CRUD operations
    db.users.updateOne(
        {
            age: {
                $lte: 17
            } 
        },
        {
            $set: {
                firstName: "Chris",
                lastName: "Mortel"
            }
        }
    );

    db.users.deleteOne(
        {
            $and: [
                {firstName: "Chris"},
                {lastName: "Mortel"}
            ]
        }
    );